@ECHO off
SET PATH=%PATH%;"C:\Program Files\Git\bin"
SET BRANCH=prod

IF "%1"=="" (
	ECHO Invalid parameter! Please specify repo name. eg: ui-grandview
	GOTO :Usage
)

IF "%2"=="" (
	ECHO Invalid parameter! Please specify portal name. eg: ewf_gvmc
	GOTO :Usage
)

IF NOT "%3"=="" (
	SET BRANCH=%3
)

ECHO ==============================================================
ECHO ================= %1 ==================
ECHO ==============================================================
FOR /f "tokens=2-4 delims=/ " %%a IN ('date /t') DO (set mydate=%%c%%a%%b)
FOR /f "tokens=1-2 delims=/: " %%a IN ('time /t') DO (set mytime=%%a%%b)
ECHO Timestamp: %mydate%%mytime%

ECHO.
ECHO Cleaning old repo folder...
rd /S /Q %1

ECHO.
ECHO Cloning from git...
git clone --branch=%BRANCH% https://acesomonkeyread@bitbucket.org/aceso/%1.git

ECHO.
ECHO Updating package version...
pushd %1
FOR /F "tokens=*" %%a IN ('git rev-list --count HEAD') DO SET HASHNUM=%%a
SET COMMIT=%HASHNUM:~0,7%
ECHO %COMMIT%
..\fart -r "package.json" "$(COMMIT)" "%COMMIT%"
rd /S /Q %1\.git
del /F /Q %1\.gitignore
popd

ECHO.
ECHO Backing up production code...
mkdir E:\WFES\ZQ-Apps\backup\%mydate%%mytime%\%2
xcopy E:\WFES\ZQ-Apps\%2 E:\WFES\ZQ-Apps\backup\%mydate%%mytime%\%2 /e /y /q
ECHO.
ECHO Backed up to E:\WFES\ZQ-Apps\backup\%mydate%%mytime%

ECHO.
ECHO Deploying...
XCOPY /E /Y /I %1\* E:\WFES\ZQ-Apps
rd /S /Q %1

ECHO.
ECHO Code has been deployed successfully!
pause
GOTO Exit

:Usage
ECHO.
ECHO deploygit REPOSITORY PORTAL
ECHO.
ECHO Example:
ECHO deploygit ui-grandview ewf_gvmc
ECHO.

:Exit
@ECHO on

